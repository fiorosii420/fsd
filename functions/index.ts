//Imports the Google Cloud client library
const Datastore = require('@google-cloud/datastore').Datastore;
// // Creates a client
const datastore = new Datastore({
    projectId: 'fsdlab08',
    keyFilename: 'fsdlab08-1672d8dfb360.json'
});
export function helloWorld(req, res) {
    res.set({ 'Access-Control-Allow-Origin': '*' });
    if (req.method === 'OPTIONS') {
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
    }
    else {
        res.send({ msg: 'Hello, World' });
    }
}

;
