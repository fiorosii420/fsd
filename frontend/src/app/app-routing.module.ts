import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './login/login.component'
import { UploadComponent } from './upload/upload.component';
import { CloudComponent } from './cloud/cloud.component';
const routes: Routes = [
  {path:"login", component:LoginComponent},
  {path:"upload", component:UploadComponent},
  {path:"cloud", component:CloudComponent},
  {path:"", redirectTo:"cloud" , pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
