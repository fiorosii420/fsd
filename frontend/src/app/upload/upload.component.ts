import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  chosenFile=null;
  constructor(private http: HttpClient) { }
  imgLocation="assets/images/placeholderImg.png"
  onSelected(event:any){
    if(event.target.files){
      var freader=new FileReader()
      freader.readAsDataURL(event.target.files[0])
      freader.onload=(event: any) => {
        this.imgLocation=event.target.result
      }
    }
  }
  ngOnInit(): void {
  }

}
