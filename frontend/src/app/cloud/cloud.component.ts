import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-cloud',
  templateUrl: './cloud.component.html',
  styleUrls: ['./cloud.component.scss']
})
export class CloudComponent implements OnInit {

  constructor(private http: HttpClient) { }

  out:any;
  ngOnInit(): void {
    this.get();
    console.log(this.out);
  }

   get(){
     this.http.get("https://europe-central2-fsdlab08.cloudfunctions.net/helloWorld").subscribe((varr:any) =>{
      this.out=varr.msg;
    })
  }


}
